<?php
require_once 'rentObject.php';
require_once 'classes/House.php';
require_once 'classes/Apartment.php';
require_once 'classes/HotelRoom.php';
require_once 'classes/HtmlProductWriter.php';

$id = $_GET['id'];
$rentObjs = null;
switch ($rentObjects[$id]['type']){
    case "hotel_room":
        $rentObjs = new HotelRoom($rentObjects[$id]['type'], $rentObjects[$id]['address'], $rentObjects[$id]['price'], $rentObjects[$id]['description'], $rentObjects['roomNumber']);
        break;
    case "apartment":
        $rentObjs = new Apartment($rentObjects[$id]['type'], $rentObjects[$id]['address'], $rentObjects[$id]['price'], $rentObjects[$id]['description'], $rentObjects[$id]['kitchen']);
        break;
    case "house":
        $rentObjs = new House($rentObjects[$id]['type'], $rentObjects[$id]['address'], $rentObjects[$id]['price'], $rentObjects[$id]['description'], $rentObjects[$id]['roomAmount']);
        break;

}
$htmlWriter = new HtmlWriter();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Details</title>
</head>
<body>
    <?php switch ($rentObjects[$id]['type']):
    case "hotel_room":?>
        <?= $htmlWriter->writeHotelRoom($rentObjs)?>
    <?php break;?>
    <?php case "house":?>
        <?= $htmlWriter->writeHouse($rentObjs)?>
    <?php break;?>
        <?php case "apartment":?>
        <?= $htmlWriter->writeApartment($rentObjs)?>
    <?php break;?>
    <?php endswitch?>
    <a href="index.php">Back</a>


</body>
</html>

