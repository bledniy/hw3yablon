<?php
require_once 'rentObject.php';
require_once 'classes/Objects.php';
require_once 'classes/House.php';
require_once 'classes/Apartment.php';
require_once 'classes/HotelRoom.php';



$rentObjs = [];
foreach ($rentObjects as $key => $rentObject) {
    $objectType = $rentObject['type'];
    $kitchen = $rentObject['kitchen'];
    if ($objectType == 'apartment') {
        $rentObjs[] = new Apartment($rentObject['type'], $rentObject['address'], $rentObject['price'], $rentObject['description'], $rentObject['kitchen']);

    }elseif ($objectType == 'house') {
        $rentObjs[] = new House($rentObject['type'], $rentObject['address'], $rentObject['price'], $rentObject['description'], $rentObject['roomAmount']);

    }elseif ($objectType == 'hotel_room') {
        $rentObjs[] = new HotelRoom($rentObject['type'], $rentObject['address'], $rentObject['price'], $rentObject['description'], $rentObject['roomNumber']);

    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
        <?php foreach ($rentObjs as $key => $rentObj): ?>
            <?=$rentObj->getSummaryLine()?>
            <a href="details.php?id=<?=$key?>">Details</a>
        <?php endforeach;?>

</body>
</html>

