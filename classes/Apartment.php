<?php


class Apartment
{
    protected $type = '';
    protected $address = '';
    protected $price = 0;
    protected $description = '';
    public $kitchen = false;

    public function  __construct($type,$address,$price,$description,$kitchen)
    {
        $this->type = $type;
        $this->address = $address;
        $this->price = $price;
        $this->description = $description;
        $this->kitchen = $kitchen;
    }
    public function getSummaryLine(){
        $kitchen = 'Нет';
        if ($this->kitchen){
            $kitchen = 'Да';
        }
        $list = '<div>';
        $list .= '<ol><li> Type:' .$this->getType() . '</li>';
        $list .= '<li> Address:' . $this->getAddress() . '</li>';
        $list .= '<li> Price:' . $this->getPrice() . '</li>';
        $list .= '<li> Kitchen:' . $kitchen . '</li></ol>';
        $list .= '</div>';
        return $list;

    }

    public function isKitchen()
    {
        return $this->kitchen;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


}