<?php


class House
{
    protected $type = '';
    protected $address = '';
    protected $price = 0;
    protected $description = '';
    protected $rooms = 0;

    public function  __construct($type,$address,$price,$description,$rooms)
    {
        $this->type = $type;
        $this->address = $address;
        $this->price = $price;
        $this->description = $description;
        $this->rooms = $rooms;
    }
    public function getSummaryLine(){
        $list = '<ol><li> Type:' .$this->type . '</li>';
        $list .= '<li> Address:' . $this->address . '</li>';
        $list .= '<li> Price:' . $this->price . '</li>';
        $list .= '<li> Rooms:' . $this->rooms . '</li></ol>';
        return $list;

    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getRooms()
    {
        return $this->rooms;
    }


}