<?php


class Objects
{
    protected $type = '';
    protected $address = '';
    protected $price = 0;
    protected $description = '';


    public function __construct($type, $address, $price, $description)
    {
        $this->type = $type;
        $this->address = $address;
        $this->price = $price;
        $this->description = $description;
    }
    public function getSummaryLine()
    {
        $list = '<ol><li> Type:' . $this->type . '</li>';
        $list .= '<li> Address:' . $this->address . '</li>';
        $list .= '<li> Price:' . $this->price . '</li>';
        $list .= '<li> Room Number:' . $this->roomNumber . '</li></ol>';
        return $list;
    }
        public function getType()
    {
        return $this->type;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getDescription()
    {
        return $this->description;
    }


}