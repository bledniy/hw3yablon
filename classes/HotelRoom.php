<?php


class HotelRoom
{
    protected $type = '';
    protected $address = '';
    protected $price = 0;
    protected $description = '';
    protected $roomNumber = 0;

    public function  __construct($type,$address,$price,$description,$roomNumber)
    {
        $this->type = $type;
        $this->address = $address;
        $this->price = $price;
        $this->description = $description;
        $this->roomNumber = $roomNumber;
    }
    public function getSummaryLine(){
        $list = '<ol><li> Type:' .$this->type . '</li>';
        $list .= '<li> Address:' . $this->address . '</li>';
        $list .= '<li> Price:' . $this->price . '</li>';
        $list .= '<li> Room Number:' . $this->roomNumber . '</li></ol>';
        return $list;

    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getRoomNumber()
    {
        return $this->roomNumber;
    }

}