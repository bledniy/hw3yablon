<?php


class HtmlWriter
{
    public function writeHotelRoom(HotelRoom $house){
        $html = '<div>';
        $html .= '<ol><li> Type:' .$house->getType() . '</li>';
        $html .= '<li> Address:' . $house->getAddress() . '</li>';
        $html .= '<li> Price:' . $house->getPrice() . '</li>';
        $html .= '<li> Description:' . $house->getDescription()  . '</li>';
        $html .= '<li> Room Number:' . $house->getRoomNumber() . '</li></ol>';
        $html .= '</div>';
        return $html;
    }
    public function writeApartment(Apartment $apartment){
        $kitchen = 'Нет';
        if ($apartment->kitchen){
            $kitchen = 'Да';
        }
        $html = '<div>';
        $html .= '<ol><li> Type:' . $apartment->getType() . '</li>';
        $html .= '<li> Address:' . $apartment->getAddress() . '</li>';
        $html .= '<li> Price:' . $apartment->getPrice() . '</li>';
        $html .= '<li> Description:' . $apartment->getDescription()  . '</li>';
        $html .= '<li> Kitchen:' . $kitchen . '</li></ol>';
        $html .= '</div>';
        return $html;
    }
    public function writeHouse(House $house){
        $html = '<div>';
        $html .= '<ol><li> Type:' .$house->getType() . '</li>';
        $html .= '<li> Address:' . $house->getAddress() . '</li>';
        $html .= '<li> Price:' . $house->getPrice() . '</li>';
        $html .= '<li> Description:' . $house->getDescription()  . '</li>';
        $html .= '<li> Rooms:' . $house->getRooms() . '</li></ol>';
        $html .= '</div>';
        return $html;
    }

}